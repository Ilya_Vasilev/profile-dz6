package ru.sber.spring.Java13Spring;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import ru.sber.spring.Java13Spring.dbexample.MyDBApplicationContext;
import ru.sber.spring.Java13Spring.dbexample.dao.BookDAO;
import ru.sber.spring.Java13Spring.dbexample.dao.UserDAO;
import ru.sber.spring.Java13Spring.dbexample.model.User;

import java.sql.SQLException;
import java.time.LocalDate;

@SpringBootApplication
public class Java13SpringApplication implements CommandLineRunner{
    private BookDAO bookDAO;
    private UserDAO userDAO;

    @Autowired
    public void setBookDAO(BookDAO bookDAO) {
        this.bookDAO = bookDAO;

    }
    @Autowired
    public void setUserDAO(UserDAO userDAO) {
        this.userDAO = userDAO;
    }
    public static void main(String[] args) {
        SpringApplication.run(Java13SpringApplication.class, args);

    }

    @Override
    public void run(String... args) throws SQLException {

        System.out.println(bookDAO.findBookById(2));

        userDAO.addUserId(new User(1, "Васильев", "Илья", LocalDate.of(1998,1,25),
                "8-888-888-88-88", "vs@mail.ru", "Недоросль, Незнакомка, Норвежский лес"));

      userDAO.getEm("vs@mail.ru");
    }
}
