package ru.sber.spring.Java13Spring.dbexample.constans;

public interface DataBaseConsts {

    String DB_HOST = "localhost";
    String DB = "local_db";
    String USER = "postgres";
    String PASSWORD = "12345";
    String PORT = "5432";
}
