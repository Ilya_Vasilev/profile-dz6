package ru.sber.spring.Java13Spring.dbexample.dao;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import ru.sber.spring.Java13Spring.dbexample.model.Book;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

@Component
public class BookDAO {
    private final Connection connection;


    public BookDAO(Connection connection) {
        this.connection = connection;
    }

    public Book findBookById(Integer bookId) throws SQLException {
        PreparedStatement selectQuery = connection.prepareStatement("select * from books where id = ?");
        selectQuery.setInt(1, bookId);
        ResultSet resultSet = selectQuery.executeQuery();
        Book book = new Book();
        while (resultSet.next()) {
            book.setBookId(resultSet.getInt("id"));
            book.setBookTitle(resultSet.getString("title_books"));
            book.setBookAuthor(resultSet.getString("book_author"));
            book.setDateAdded(resultSet.getDate("date_date"));
        }
        return book;
    }


    public void infoBook(String[] title) throws SQLException {
        for (String str : title) {
            PreparedStatement selectQuery = connection.prepareStatement("select * from books where title_books = ?");
            selectQuery.setString(1, str);
            ResultSet resultSet = selectQuery.executeQuery();
            Book book = new Book();
            while (resultSet.next()) {
                book.setBookId(resultSet.getInt(1));
                book.setBookTitle(resultSet.getString(2));
                book.setBookAuthor(resultSet.getString(3));
                book.setDateAdded(resultSet.getDate(4));
                System.out.println(book);
            }

        }
   }
}


