package ru.sber.spring.Java13Spring.dbexample.dao;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import ru.sber.spring.Java13Spring.dbexample.model.User;

import java.sql.*;


@Component
@Scope("prototype")
public class UserDAO {
    private final Connection connection;
    private final BookDAO bookDAO;

    public UserDAO(Connection connection, BookDAO bookDAO) {
        this.connection = connection;
        this.bookDAO = bookDAO;
    }


    public void addUserId(User user) throws SQLException {
        PreparedStatement addQuery = connection.prepareStatement("insert into users" +
                "(surname, name_name, date_date, telephone, email, list_of_books)" + "values (?,?,?,?,?,?,?)");
        addQuery.setString(1, user.getUserSurname());
        addQuery.setString(2, user.getUserName());
        addQuery.setDate(3, Date.valueOf(user.getUserDate()));
        addQuery.setString(4, user.getUserPhone());
        addQuery.setString(5, user.getUserEmail());
        addQuery.setString(6, user.getUserListOfBooks());
    }

    public void getEm(String email) throws SQLException {
        PreparedStatement selectQuery = connection.prepareStatement("select list_of_books from users where email = ?");
        selectQuery.setString(1, email);
        ResultSet resultSet = selectQuery.executeQuery();
        while (resultSet.next()) {
            String result = resultSet.getString("list_of_books");
            String[] str = result.split(", ");
            bookDAO.infoBook(str);
        }
    }
}


