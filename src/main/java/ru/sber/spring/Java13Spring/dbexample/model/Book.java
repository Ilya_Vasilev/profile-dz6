package ru.sber.spring.Java13Spring.dbexample.model;

import lombok.*;

import java.util.Date;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
@Getter
@Setter
public class Book {
    private Integer bookId;
    private String bookTitle;
    private String bookAuthor;
    private Date dateAdded;
}


