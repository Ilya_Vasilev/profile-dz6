package ru.sber.spring.Java13Spring.dbexample.model;

import lombok.*;
import java.time.LocalDate;

@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor

public class User {
    private Integer userId;
    private String userSurname;
    private String userName;
    private LocalDate userDate;
    private String userPhone;
    private String userEmail;
    private String userListOfBooks;
}
