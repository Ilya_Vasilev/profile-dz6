
create table users
(
    id serial primary key,
    surname varchar(20) not null,
    name_name varchar(15) not null,
    date_date date not null,
    telephone varchar(15) unique,
    email varchar(25) unique,
    list_of_books varchar(1000)
);

select * from users;
commit

create table books
(
    id serial primary key,
    title_books varchar(50) not null,
    book_author varchar(50) not null,
    date_date timestamp not null
);
select * from books;
insert into books(title_books,book_author,date_date)
values ('Недоросль', 'Д. И. Фонвизин', now());
insert into books(title_books,book_author,date_date)
values ('Незнакомка', 'А.А. Блок',now());
insert into books (title_books,book_author,date_date)
values ('Норвежский лес','Х. Мураками', now());
commit;
